%define name 	gds-gui
%define version 3.0.0
%define release 1.3
%define daswg   /usr
%define prefix  %{daswg}
%define withpygds 1
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

%define root_cling %(test ! \\( "`root-config --has-cling`" == "yes" -o "`root-config --version | sed -e 's/[.].*$//g'`" -ge 6 \\); echo $?)

Name: 		%{name}
Summary: 	gds-gui 3.0.0
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gds-base-devel >= 3.0.0
BuildRequires:  gzip
BuildRequires:  bzip2
BuildRequires:  libXpm-devel
BuildRequires:  root
BuildRequires:  readline-devel
BuildRequires:  fftw-devel
BuildRequires:  jsoncpp-devel
Requires: gds-base >= 3.0.0
Requires: jsoncpp-devel
Prefix:		      %prefix

%description
GDS GUI programs

%package headers
Summary: 	GDS monitor header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base-headers >= 3.0.0

%description headers
GDS monitor header files.

%package crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base >= 3.0.0
Requires: gds-base-headers >= 3.0.0
Requires: root

%description crtools
GDS control room tools

%package devel
Summary: 	GDS monitor development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base-devel >= 3.0.0
Requires: %{name}-headers = %{version}-%{release}

%description devel
GDS GUI development files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

%if 0%{?rhel} < 8
CXXFLAGS="-std=c++11"
export CXXFLAGS
%endif

export PKG_CONFIG_PATH ROOTSYS
./configure  \
    --prefix=%prefix \
    --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/gds \
	  --enable-online \
    --enable-dtt
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files
%defattr(-,root,root)

%files crtools
%defattr(-,root,root)
%{_libdir}/*.so.*
%if %{root_cling}
%{_libdir}/*_rdict.pcm
%endif

%files headers
%defattr(-,root,root)
%{_includedir}/gds

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig

%changelog
* Sat Nov 19 2022 Edward Maros <ed.maros@ligo.org> - 3.0.0-1
- Updated for release as described in NEWS.md

* Wed Nov 02 2022 Edward Maros <ed.maros@ligo.org> - 2.19.8-1
- Updated for release as described in NEWS.md

* Fri Jan 28 2022 Edward Maros <ed.maros@ligo.org> - 2.19.7-1
- Updated for release as described in NEWS.md

* Thu Aug 12 2021 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- Updated for release as described in NEWS.md

* Thu Jun 24 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Split lowlatency as a separate package
